//finding the area of a disk with radius r

#include<stdio.h>

float r,area;
const float pi = 3.14;

int main()
{
	printf("Enter the radius of the disk");
	scanf("%f",&r);

	area = (pi * (r*r));

	printf("THE AREA OF THE DISK WITH THE RADIUS : %.2f IS %.2f",r,area);

	



	return 0;
}